## Abstract Factory

**Fabryka abstrakcyjna** (ang. Abstract Factory) – kreacyjny wzorzec projektowy, 
którego celem jest dostarczenie interfejsu do tworzenia różnych obiektów jednego 
typu (tej samej rodziny) bez specyfikowania ich konkretnych klas. Umożliwia 
jednemu obiektowi tworzenie różnych, powiązanych ze sobą, reprezentacji podobiektów 
określając ich typy podczas działania programu. Fabryka abstrakcyjna różni się 
od Budowniczego tym, że kładzie nacisk na tworzenie produktów z konkretnej rodziny, 
a Budowniczy kładzie nacisk na sposób tworzenia obiektów.

**Struktura**

Jak widać na załączonym diagramie klas wzorzec zbudowany jest z kilku podstawowych klas. 
Klasa ComputerAbstractFactory deklaruje abstrakcyjny interfejs umożliwiający tworzenie produktów. 
Interfejs ten jest implementowany w konkretnych fabrykach, które odpowiedzialne są za tworzenie 
konkretnych produktów.

![alt text](https://cdn.journaldev.com/wp-content/uploads/2013/06/Abstract-Factory-Pattern.png)

**Korzyści stosowania wzorca**

* możliwość ukrycia szczegółów implementacyjnych klas reprezentujących konkretny produkt - klient widzi tylko interfejs.
* wzorzec ten to "fabryka fabryk", którą można łatwo rozszerzyć, aby pomieścić więcej produktów
* wzorzec jest solidny i unika warunkowej logiki wzorca Fabryki

Źródła  
* https://pl.wikipedia.org/wiki/Fabryka_abstrakcyjna_(wzorzec_projektowy)
* https://www.journaldev.com/1418/abstract-factory-design-pattern-in-java

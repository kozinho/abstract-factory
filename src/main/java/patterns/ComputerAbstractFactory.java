package main.java.patterns;

public interface ComputerAbstractFactory {

    public Computer createComputer();

}
